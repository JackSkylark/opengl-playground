# Playground for OpenGL

Just a playground for stuff while [learning OpenGL](https://learnopengl.com/).

## How do I run this thing?

```
cmake .
make
./build/main
```

Or try one of the examples

```
./build/example-gears
```

## How do I setup this thing?

### Setting up `glfw`

We opted to build and install [`glfw`](https://github.com/glfw/glfw) from
source.

#### Linux dependencies [(source)](https://www.glfw.org/docs/latest/compile.html#compile_compile)

```
sudo apt install xorg-dev
```

#### Clone, build, install

```
git clone git@github.com:glfw/glfw.git
git checkout 3.3-stable
cmake -DBUILD_SHARED_LIBS=ON -DGLFW_BUILD_EXAMPLES=OFF -DGLFW_BUILD_TESTS=OFF -DGLFW_BUILD_DOCS=OFF .
make install
```

### Setting up `glad`

**NOTE:** This step is no longer needed as these files have been committed to the repository.

Glad generates some boilerplate code based on the graphics spec and language.

#### Install python lib

Make sure you have python version 3 and `pip` is using `python3`

```
pip install --user glad
```

#### Generate

```
cd glad
glad --spec=gl --generator=c --out-dir=.
```
